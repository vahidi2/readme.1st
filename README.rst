
I work at the `SICS <https://en.wikipedia.org/wiki/Swedish_Institute_of_Computer_Science>`_ security lab (now part of `RI.SE  <https://www.ri.se/>`_ ). If you are looking for repositories related to my work please try these places:

* Most of my work is on the `SICS security lab team <https://bitbucket.org/sicssec/>`_ page on bitbucket.
* Some repositories on my `private account <https://bitbucket.org/vahidi/>`_ on bitbucket are also used at work.
* My bitbucket `work account <https://bitbucket.org/vahidi2/>`_ on bitbucket is mostly empty.
* My github `work account <https://github.com/avahidi/>`_  also mostly empty.
* For now, you may also find the github `RISE/SICS organization <https://github.com/RISE-SICS>`_ mostly empty.

Another place I almost never visit is my `LinkedIn page <https://www.linkedin.com/in/arashvahidi>`_ ...

